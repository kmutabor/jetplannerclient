package ru.lair.leadxg.jetplanner.util;


import java.util.ArrayList;
import java.util.Arrays;

public interface Constants {

    /*
    Константы адресов запросов.
    Должны быть идентичны таким же контсантам на стороне сервера
    Но здесь без ведущего слеша '/'
     */
    interface Request{
        String USER = "1e6de126544afe13d60b9480e6410925";
        String JOB = "55322413a05ae9a1540dfd7d7f8d7759";
        String EVENT = "dd37107814f62ad88b8a5a00e1289e63";
        String ADDRESS = "937df55b315862dfe4b5405b3f14eec6";
    }

    /*
        MENU ELEMENTS
     */
    int MENU_ADD = 100;
    int MENU_UPDATE = 200;
    int MENU_DELETE = 300;

    int MENU_JOB_ACTIVITY = 1;
    int MENU_EVENT_ACTIVITY = 2;

    /*
       DIALOG
     */
    int DIALOG_DELETE_CONFIRM = 10;
    int DIALOG_EVENT_CREATION_CONFIRM = 20;
    int DIALOG_EVENT_STATUS_UPDATE = 20;

    /*
        USER
     */
    String USER_DATA_PREFERENCE = "UserDataPreference";
    String USER_ID = "user_id";
    String USER_AUTH_TOKEN = "user_auth_token";
    String USER_JOB_COUNT = "job_counter";
    String ADDRESS_COUNT = "address_counter";
    String EVENT_COUNT = "event_counter";

    /*
        MIN/MAX VALUES
     */
    int MAX_JOB_COUNTER = 5;
    int MAX_EVENT_COUNTER = 20;
    int MAX_ADDRESS_COUNTER = 5;

    interface EventStatus{
        String REQUEST = "request";
        String APPROVED = "approved";
        String CANCELLED = "cancelled";
        String DONE =  "done";
        String EXPIRED =  "expired";
    }


    /*
        WORK HOUR DIAPASON
     */
    interface WorkHour{
        int START = 8;
        int END = 16;

        ArrayList<Integer> AVAILABLE_HOURS = new ArrayList<Integer>(Arrays.asList(new Integer[]{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}));

    }

    /*
        TIME INTERVAL
     */
    interface TimeInterval{
        int _60 = 60;
        int _120 = 120;
        int _180 = 180;
    }


    int ADDRESS_RESULT = 1;
    int ADDRESS_UPDATING_RESULT = 2;


    /*
        LOG PREFIX
     */
    String LOG_PREFIX = "TAG_";

}
