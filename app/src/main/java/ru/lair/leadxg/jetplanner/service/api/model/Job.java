package ru.lair.leadxg.jetplanner.service.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Job implements Serializable{

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("userId")
    @Expose
    private int userId;
    @SerializedName("addressId")
    @Expose
    private int addressId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("amount")
    @Expose
    private int amount;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("timeInterval")
    @Expose
    private int timeInterval;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("address")
    @Expose
    private Address address;
    @SerializedName("eventCount")
    @Expose
    private int eventCount;
    @SerializedName("workHourStart")
    @Expose
    private int workHourStart;
    @SerializedName("workHourEnd")
    @Expose
    private int workHourEnd;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(int timeInterval) {
        this.timeInterval = timeInterval;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public int getEventCount() {
        return eventCount;
    }

    public void setEventCount(int eventCount) {
        this.eventCount = eventCount;
    }

    public int getWorkHourStart() {
        return workHourStart;
    }

    public void setWorkHourStart(int workHourStart) {
        this.workHourStart = workHourStart;
    }

    public int getWorkHourEnd() {
        return workHourEnd;
    }

    public void setWorkHourEnd(int workHourEnd) {
        this.workHourEnd = workHourEnd;
    }

    public String toString(){
        return this.getId() + " : " + this.getUserId() + " : "
                + this.getAddressId() + " : " + this.getName() + " : " + this.getAmount() + " : " + this.getDescription() + " : "
                + this.getTimeInterval() + " : " + this.getUser() + " : " + this.getEventCount()+ " : " + this.getWorkHourStart()+ " : " + this.getWorkHourEnd();
    }

}

