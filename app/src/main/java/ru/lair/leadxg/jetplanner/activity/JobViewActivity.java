package ru.lair.leadxg.jetplanner.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.lair.leadxg.jetplanner.R;
import ru.lair.leadxg.jetplanner.service.api.model.Event;
import ru.lair.leadxg.jetplanner.util.Constants;
import ru.lair.leadxg.jetplanner.util.PreferencesHelper;
import ru.lair.leadxg.jetplanner.util.RetroManager;

public class JobViewActivity extends AppCompatActivity implements View.OnClickListener, TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    private final String TAG = Constants.LOG_PREFIX + getClass().getSimpleName();

    private PreferencesHelper preferencesHelper;
    private int userId;
    private String token;
    private String jobOwner;
    private String jobName;
    private String jobAddress;
    private String jobDesc;
    private String dateAndTimeResult;
    private Calendar calendar;
    private int jobId;
    private int timeInterval;
    private int workHourStart;
    private int workHourEnd;

    private TextView mJobAboutOwner;
    private TextView mJobAboutName;
    private TextView mJobAboutAddress;
    private TextView mJobAboutDesc;
    private TextView mDateTimeResultView;
    private Button mJobAboutSubmit;

//    private boolean isDateFullBusyFlag = false;

    private int selectedYear;
    private int selectedMonth;
    private int selectedDay;
    private int selectedHour;
    private int selectedMinute;
    private String selectedDate;
    private String selectedTime;

    // дата yyyy-MM-dd, список занятых часов и минут
    private Map<String, List<String>> eventDateMap = new HashMap<>();

    private RetroManager retroManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_about);

        Intent jobsIntent = getIntent();
        jobOwner = jobsIntent.getStringExtra("jobAboutOwner");
        jobName = jobsIntent.getStringExtra("jobAboutName");
        jobAddress = jobsIntent.getStringExtra("jobAboutAddress");
        jobDesc = jobsIntent.getStringExtra("jobAboutDesc");
        jobId = jobsIntent.getIntExtra("jobId", 0);
        timeInterval = jobsIntent.getIntExtra("timeInterval", 0);
        workHourStart = jobsIntent.getIntExtra("work_hour_start", 0);
        workHourEnd = jobsIntent.getIntExtra("work_hour_end", 0);

        preferencesHelper = PreferencesHelper.getInstance(getSharedPreferences(Constants.USER_DATA_PREFERENCE, MODE_PRIVATE));
        token = preferencesHelper.getToken();
        userId = preferencesHelper.getUserId();

        retroManager = RetroManager.getInstance();

        retroManager.getEventsByJobIdForClient(userId, token, jobId, true).enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
                List<Event> list = response.body();
                if(list.get(0).getId() == 0){return;}
                for (Event e : list) {
                    String dateTime = e.getDesiredTime();

                    String date = dateTime.substring(0, dateTime.lastIndexOf(' ')).trim();
                    String time = dateTime.substring(dateTime.lastIndexOf(' '), dateTime.lastIndexOf(':')).trim();

                    Log.e(TAG,"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                    Log.e(TAG,date + " " + time);
                    if (!eventDateMap.containsKey(date)) {
                        eventDateMap.put(date, new ArrayList<String>());
                    }
                    eventDateMap.get(date).add(time);
                }
            }

            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
                Log.e(TAG, "Нет доступа к апи");
                finish();
            }
        });

        mJobAboutOwner = (TextView) findViewById(R.id.jobAboutOwner);
        mJobAboutName = (TextView) findViewById(R.id.jobAboutName);
        mJobAboutAddress = (TextView) findViewById(R.id.jobAboutAddress);
        mJobAboutDesc = (TextView) findViewById(R.id.jobAboutDesc);
        mDateTimeResultView = (TextView) findViewById(R.id.dateTimeResultView);
        mJobAboutSubmit = (Button) findViewById(R.id.toCalendarBtn);

        mJobAboutSubmit.setOnClickListener(this);

        mJobAboutOwner.setText(jobOwner);
        mJobAboutName.setText(jobName);
        mJobAboutAddress.setText(jobAddress);
        mJobAboutDesc.setText(jobDesc);

    }

    @Override
    public void onResume() {
        freeResources();
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toCalendarBtn:
                if (preferencesHelper.getEventCounter() >= Constants.MAX_EVENT_COUNTER) {
                    Toast.makeText(JobViewActivity.this, R.string.event_over_limit_message, Toast.LENGTH_SHORT).show();
//                    JobViewActivity.super.onBackPressed();
                    return;
                }
                dateSet();
                break;
        }
    }

    public void dateSet() {
        freeResources();
        calendar = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                JobViewActivity.this,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setThemeDark(true);
//                dpd.vibrate(true);

        //показывать год первым
        dpd.showYearPickerFirst(false);


        dpd.setVersion(DatePickerDialog.Version.VERSION_2);
        dpd.setAccentColor(getResources().getColor(R.color.colorPrimaryDark));
        dpd.setTitle("Выберите дату:");

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH)+1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);

        //отключаем сегодняшнее число
        Calendar[] days;
        String dateNow = year+"-"+(month<10?"0"+month:month)+"-"+(day<10?"0"+day:day);

        if (hour >= workHourEnd || isFilledAllAfterHour(dateNow, hour)) {
            int ignoreToday = 1;
            days = new Calendar[27];
            for (int i = 0; i < 27; i++) {
                calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_MONTH, ignoreToday++);
                days[i] = calendar;
            }
            dpd.setSelectableDays(days);
        } else {
            days = new Calendar[28];
            for (int i = 0; i < 28; i++) {
                calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_MONTH, i);
                days[i] = calendar;
            }
            dpd.setSelectableDays(days);
        }

        List<Calendar> calendarList = new ArrayList<>();
        for (Map.Entry<String, List<String>> entry: eventDateMap.entrySet()) {
            String date = entry.getKey();
            if(isDateFullBusy(date)){
                SimpleDateFormat sdf = null;
                Calendar cal = Calendar.getInstance();
                sdf = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    cal.setTime(sdf.parse(date));
                    calendarList.add(cal);
                } catch (ParseException e) {
                    e.printStackTrace();
                    dateSet();
                }
                Log.e(TAG, "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                Log.e(TAG, cal.get(Calendar.YEAR)+"");
                Log.e(TAG, cal.get(Calendar.MONTH)+1+"");
                Log.e(TAG, cal.get(Calendar.DAY_OF_MONTH)+"");

            }
        }
        if(!calendarList.isEmpty()){
            days = calendarList.toArray(new Calendar[0]);
            dpd.setDisabledDays(days);
        }

        dpd.show(getFragmentManager(), "Datepickerdialog");
    }


    /**
     *
     * @param hour
     * @return
     */
    private int getHourIncrement(int hour){
        switch (timeInterval){
            case Constants.TimeInterval._60:
                return hour+1;
            case Constants.TimeInterval._120:
                if(workHourStart % 2 == 0){
                    return hour+2;
                }else{
                    return hour+1;
                }
            case Constants.TimeInterval._180:
                if(workHourStart % 2 == 0){
                    switch (hour){
                        case 0:
                        case 3:
                        case 6:
                        case 9:
                        case 12:
                        case 15:
                        case 18:
                            return hour+3;
                        case 1:
                        case 4:
                        case 7:
                        case 10:
                        case 13:
                        case 16:
                        case 19:
                            return hour+2;
                        case 2:
                        case 5:
                        case 8:
                        case 11:
                        case 14:
                        case 17:
                        case 20:
                            return hour+1;

                    }
                }else{
                    switch (hour){
                        case 0:
                        case 3:
                        case 6:
                        case 9:
                        case 12:
                        case 15:
                        case 18:
                            return hour+1;
                        case 1:
                        case 4:
                        case 7:
                        case 10:
                        case 13:
                        case 16:
                        case 19:
                            return hour+3;
                        case 2:
                        case 5:
                        case 8:
                        case 11:
                        case 14:
                        case 17:
                        case 20:
                            return hour+2;
                    }
                }
                break;
        }
        return hour;
    }

    /**
     *
     * @param date
     * @param hour
     * @return
     */
    private boolean isFilledAllAfterHour(String date, int hour){
        if(eventDateMap.containsKey(date)){
            List<String> stringList = eventDateMap.get(date);
            int hi = getHourIncrement(hour);
            switch (timeInterval){
                case Constants.TimeInterval._60:
                    for(int i = hi; i < workHourEnd; i++){
                        String h = (i < 10 ? "0" + i : i)+":00";
                        if(!stringList.contains(h)){
                            return false;
                        }
                    }
                    break;
                case Constants.TimeInterval._120:
                    for(int i = hi; i < workHourEnd && i < 23; i += 2){
                        String h = "" + (i < 10 ? "0" + i : i) + ":00";
                        Log.e(TAG, h);
                        if(!stringList.contains(h)){
                            return false;
                        }
                    }
                    break;
                case Constants.TimeInterval._180:
                    for(int i = hi; i < workHourEnd && i < 22; i += 3){
                        String h1 = (i < 10 ? "0" + i : i) + ":00";
                        Log.e(TAG, h1);
                        if(!stringList.contains(h1)){
                            return false;
                        }
                    }
                    break;
            }
            return true;
        }
        return false;
    }


    /**
     *
     * @param date
     * @param hour
     * @return
     */
    private int getFirstFreeTimeAfterHour(String date, int hour){
        hour = getHourIncrement(hour);
        Log.e(TAG, "getFirstFreeTimeAfterHour: " + hour);
        if(eventDateMap.containsKey(date)){
            List<String> stringList = eventDateMap.get(date);
            int hi = getHourIncrement(hour);
            switch (timeInterval){
                case Constants.TimeInterval._60:
                    for(int i = hi; i<workHourEnd; i++){
                        String h = (i < 10 ? "0" + i : i) + ":00";
                        if(!stringList.contains(h)){
                            return Integer.parseInt(h.substring(0,2));
                        }
                    }
                    break;
                case Constants.TimeInterval._120:
                    for(int i = hi; i < workHourEnd && i < 23; i += 2){
                        String h = "" + (i < 10 ? "0" + i : i) + ":00";
                        Log.e(TAG, h);
                        if(!stringList.contains(h)){
                            return Integer.parseInt(h.substring(0,2));
                        }
                    }
                    break;
                case Constants.TimeInterval._180:
                    for(int i = hi; i < workHourEnd && i < 22; i += 3){
                        String h = (i <10 ? "0" + i : i) + ":00";
                        Log.e(TAG, h);
                        if(!stringList.contains(h)){
                            return Integer.parseInt(h.substring(0, 2));
                        }
                    }
                    break;
            }
            return hour;
        }
        return hour;
    }

//    /**
//     * Проверяет занятое время начиная с указанного часа до конца временного интервала 'workHourEnd'
//     * @param dayDate
//     * @param hour
//     * @return true , если запись свободна
//     */
//    private boolean isTimeFree(String dayDate, int hour){
//        Log.e(TAG, "@@@@@@@@@@@@###############3");
//        Log.e(TAG, dayDate + " : "+hour);
//        if(eventDateMap.containsKey(dayDate)){
//            List<String> stringList = eventDateMap.get(dayDate);
//            Log.e(TAG, timeInterval+" minute CASE");
//            switch (timeInterval){
//                case Constants.TimeInterval._60:
//                    for(int i = hour; i<workHourEnd; i++){
//                        String h = (i<10?"0"+i:i)+":00";
//                        if(!stringList.contains(h)){
//                            return true;
//                        }
//                    }
//                    break;
//                case Constants.TimeInterval._120:
//                    for(int i = hour; i<workHourEnd && i<23; i+=2){
//                        String h = ""+(i<10?"0"+i:i)+":00";
//                        Log.e(TAG, h);
//                        if(!stringList.contains(h)){
//                            return true;
//                        }
//                    }
//                    break;
//                case Constants.TimeInterval._180:
//                    for(int i = hour; i<workHourEnd && i<22; i+=3){
//                        String h1 = (i<10?"0"+i:i)+":00";
//                        if(!stringList.contains(h1)){
//                            return true;
//                        }
//                    }
//                    break;
//            }
//            return false;
//        }
//        return true;
//    }


    /**
     * Проверяет дату на заполненность заяками по времени в зависимости от интевала.
     * @param date
     * @return true (если на все даты, соответствующие временному промежутку, указанному создателем job была произведена запись.)
     */
    private boolean isDateFullBusy(String date){
        Log.e(TAG, "isDateFullBusy: " + date);
        int diff = Math.abs(workHourStart - workHourEnd);
        if (eventDateMap.containsKey(date)) {
            int size = eventDateMap.get(date).size() - 1;
            Log.e(TAG, "size: " + eventDateMap.get(date).size());
            switch (timeInterval) {
                case Constants.TimeInterval._60:
                    return diff == size;
                case Constants.TimeInterval._120:
                    return (diff / 2) == size;
                case Constants.TimeInterval._180:
                    return (((double)diff) / 3)  == size;
            }
        }
        return false;
    }

    /**
     *
     */
    public void timeSet() {
        calendar = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                //для этого конструктора необходимо было реализовать интерфейс
                this,
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                true
        );

        //темная тема (серый фон)
        tpd.setThemeDark(true);

        //вибрация (может будет нужна)
//         tpd.vibrate(true);

        //тоже настройка темы
        tpd.setVersion(TimePickerDialog.Version.VERSION_2);

        //цвет
        tpd.setAccentColor(getResources().getColor(R.color.colorPrimaryDark));

        //заголовок
        tpd.setTitle("Выберите время:");

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH)+1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);

        Log.e(TAG, "################################################");
        Log.e(TAG, "start: " + workHourStart + ", end: " + workHourEnd);
        Log.e(TAG, "DAY : " + day + ", THIS DAY : " + selectedDay + ", HOUR : " + hour);
        //проверка текущей даты (запрет выбора прошедших часов)
        if (selectedDay == day && hour > workHourStart) {
            tpd.setMinTime(getFirstFreeTimeAfterHour(year + "-" + (month < 10 ? "0" + month:month) + "-" + (day < 10 ? "0" + day : day),hour), 0, 0);
        } else {
            tpd.setMinTime(workHourStart, 0, 0);
        }
        tpd.setMaxTime(workHourEnd, 0, 0);

        setWorkDayTimeInterval(tpd);

        if(eventDateMap.containsKey(selectedDate)){
            List<String> times = eventDateMap.get(selectedDate);
            Timepoint[] disabledTime = new Timepoint[times.size()];
            for (int i = 0 ; i < times.size(); i++) {
                String dateTime = selectedDate + " " + times.get(i);
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = null;
                sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                try {
                    cal.setTime(sdf.parse(dateTime));
                    disabledTime[i] = new Timepoint(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));
                } catch (ParseException e) {
                    e.printStackTrace();
                    dateSet();
                }
            }
            tpd.setDisabledTimes(disabledTime);
        }

        tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                dateSet();
            }
        });
        tpd.show(getFragmentManager(), "Timepickerdialog");
    }



    /**
     * Заполняет досутпнымы для выбора датами календарь в зависимости от выдранного интервала
     *
     * @param tpd
     */
    private void setWorkDayTimeInterval(TimePickerDialog tpd) {
        Timepoint[] timepoints;
        switch (timeInterval) {
            //1 hr
            case Constants.TimeInterval._60:
                Log.e(TAG, "CASE 60");
                tpd.enableMinutes(false);
//                tpd.setTimeInterval(1,60);
                timepoints = new Timepoint[]{
                        new Timepoint(0),
                        new Timepoint(1),
                        new Timepoint(2),
                        new Timepoint(3),
                        new Timepoint(4),
                        new Timepoint(5),
                        new Timepoint(6),
                        new Timepoint(7),
                        new Timepoint(8),
                        new Timepoint(9),
                        new Timepoint(10),
                        new Timepoint(11),
                        new Timepoint(12),
                        new Timepoint(13),
                        new Timepoint(14),
                        new Timepoint(15),
                        new Timepoint(16),
                        new Timepoint(17),
                        new Timepoint(18),
                        new Timepoint(19),
                        new Timepoint(20),
                        new Timepoint(21),
                        new Timepoint(22),
                        new Timepoint(23)
                };
                tpd.setSelectableTimes(timepoints);
                break;
            //2 hrs
            case Constants.TimeInterval._120:
                tpd.enableMinutes(false);
                Log.e(TAG, "CASE 120");
                if ((workHourStart % 2) == 0) {
                    Log.e(TAG, "EVEN HOUR START");
                    timepoints = new Timepoint[]{
                            new Timepoint(0),
                            new Timepoint(2),
                            new Timepoint(4),
                            new Timepoint(6),
                            new Timepoint(8),
                            new Timepoint(10),
                            new Timepoint(12),
                            new Timepoint(14),
                            new Timepoint(16),
                            new Timepoint(18),
                            new Timepoint(20),
                            new Timepoint(22)
                    };
                    tpd.setSelectableTimes(timepoints);
                } else {
                    Log.e(TAG, "ODD HOUR START");
                    timepoints = new Timepoint[]{
                            new Timepoint(1),
                            new Timepoint(3),
                            new Timepoint(5),
                            new Timepoint(7),
                            new Timepoint(9),
                            new Timepoint(11),
                            new Timepoint(13),
                            new Timepoint(15),
                            new Timepoint(17),
                            new Timepoint(19),
                            new Timepoint(21),
                            new Timepoint(23)
                    };
                    tpd.setSelectableTimes(timepoints);
                }
                break;
            //3 hrs
            case Constants.TimeInterval._180:
                tpd.enableMinutes(false);
                Log.e(TAG, "CASE 180");
                switch (workHourStart) {
                    case 0:
                    case 3:
                    case 6:
                    case 9:
                    case 12:
                    case 15:
                    case 18:
                    case 21:
                        timepoints = new Timepoint[]{
                                new Timepoint(0),
                                new Timepoint(3),
                                new Timepoint(6),
                                new Timepoint(9),
                                new Timepoint(12),
                                new Timepoint(15),
                                new Timepoint(18),
                                new Timepoint(21)
                        };
                        tpd.setSelectableTimes(timepoints);
                        break;

                    case 1:
                    case 4:
                    case 7:
                    case 10:
                    case 13:
                    case 16:
                    case 19:
                    case 22:
                        timepoints = new Timepoint[]{
                                new Timepoint(1),
                                new Timepoint(4),
                                new Timepoint(7),
                                new Timepoint(10),
                                new Timepoint(13),
                                new Timepoint(16),
                                new Timepoint(19),
                                new Timepoint(22)
                        };
                        tpd.setSelectableTimes(timepoints);
                        break;

                    case 2:
                    case 5:
                    case 8:
                    case 11:
                    case 14:
                    case 17:
                    case 20:
                    case 23:
                        timepoints = new Timepoint[]{
                                new Timepoint(2),
                                new Timepoint(5),
                                new Timepoint(8),
                                new Timepoint(11),
                                new Timepoint(14),
                                new Timepoint(17),
                                new Timepoint(20),
                                new Timepoint(23)
                        };
                        tpd.setSelectableTimes(timepoints);
                        break;
                }

                break;
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        selectedYear = year;
        selectedMonth = monthOfYear + 1;
        selectedDay = dayOfMonth;
        selectedDate = selectedYear + "-" + (selectedMonth < 10 ? "0"+ selectedMonth : selectedMonth) + "-" + (selectedDay < 10 ? "0" + selectedDay : selectedDay) ;
        timeSet();
    }

    /**
     *
     */
    private void freeResources(){
        selectedYear = 0;
        selectedMonth = 0;
        selectedDay = 0;
        selectedHour = 0;
        selectedMinute = 0;
        selectedDate = "";
        selectedTime = "";
        dateAndTimeResult = "";
        calendar = null;
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        selectedHour = hourOfDay;
        selectedMinute = minute;
        selectedTime = (selectedHour < 10 ? "0" + selectedHour : "" + selectedHour) + ":" +
                (selectedMinute < 10 ? "0" + selectedMinute : "" + selectedMinute);
        dateAndTimeResult = selectedYear + "-" + selectedMonth + "-" + selectedDay + " " + selectedTime ;
                ;

        showDialog(Constants.DIALOG_EVENT_CREATION_CONFIRM);
        Log.d(TAG, "Результат выбора: " + dateAndTimeResult);
    }

    protected Dialog onCreateDialog(int id) {
        if (id == Constants.DIALOG_EVENT_CREATION_CONFIRM) {
            AlertDialog.Builder adb = new AlertDialog.Builder(this);
            adb.setTitle(R.string.user_create_event_title);
            adb.setMessage(R.string.user_create_event_dialog_message);
//            adb.setIcon(android.R.drawable.ic_dialog_info);
            adb.setPositiveButton(R.string.delete_dialog_positive_btn, createEventDialog);
            adb.setNegativeButton(R.string.delete_dialog_negative_btn, createEventDialog);
            return adb.create();
        }
        return super.onCreateDialog(id);
    }

    DialogInterface.OnClickListener createEventDialog = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    mDateTimeResultView.setText(dateAndTimeResult);
                    createEvent();
                    break;
                case Dialog.BUTTON_NEGATIVE:
                    break;
            }
        }
    };

    /**
     *
     */
    public void createEvent() {
        Log.d(TAG, "createEvent()");

        retroManager = RetroManager.getInstance();
        Log.d(TAG, userId + ":" + token + ":" + jobId);

        try {
            Call<Event> response = retroManager.addEvent(userId, token, jobId, Constants.EventStatus.REQUEST, dateAndTimeResult);
            response.enqueue(new Callback<Event>() {
                @Override
                public void onResponse(Call<Event> call, Response<Event> response) {
                    Event responseBody = response.body();
                    if(responseBody.getDesiredTime().equals("busy")){
                        Toast.makeText(JobViewActivity.this, R.string.event_create_duplicate_time_error, Toast.LENGTH_LONG).show();
                    }else if(responseBody.getId() > 0){
                        preferencesHelper.incrementEventCounter();

                        Toast.makeText(JobViewActivity.this, R.string.event_created, Toast.LENGTH_SHORT).show();
                        Log.d(TAG, responseBody.toString());
                        Log.d(TAG, "Создано: event " + responseBody.getId() + " : " + responseBody.getJobId() + " : " + responseBody.getStatus() + " : " + responseBody.getDesiredTime());

                        if(!eventDateMap.containsKey(selectedDate)){
                            eventDateMap.put(selectedDate, new ArrayList<String>());
                        }
                        eventDateMap.get(selectedDate).add(selectedTime);
                        Intent iEvent = new Intent(JobViewActivity.this, EventActivity.class);
                        startActivity(iEvent);
                    }else{
                        Toast.makeText(JobViewActivity.this, R.string.event_create_error, Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<Event> call, Throwable t) {
                    Log.e(TAG, t.getMessage());
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
