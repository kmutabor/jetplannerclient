package ru.lair.leadxg.jetplanner.service.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Event {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("userId")
    @Expose
    private int userId;
    @SerializedName("jobId")
    @Expose
    private int jobId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("desiredTime")
    @Expose
    private String desiredTime;
    @SerializedName("job")
    @Expose
    private Job job;
    @SerializedName("user")
    @Expose
    private User user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDesiredTime() {
        return desiredTime;
    }

    public void setDesiredTime(String desiredTime) {
        this.desiredTime = desiredTime;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String toString(){
        return this.getId() + " : " + this.getUserId() + " : " + this.getJob() + " : " + this.getUser() + " : " + this.getStatus() + " : " + this.getDesiredTime();
    }

}