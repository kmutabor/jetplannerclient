package ru.lair.leadxg.jetplanner.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ru.lair.leadxg.jetplanner.R;
import ru.lair.leadxg.jetplanner.service.api.model.Job;
import ru.lair.leadxg.jetplanner.util.Constants;


public class JobSearchAdapter extends RecyclerView.Adapter<JobSearchAdapter.JobHolder>{

    private final String TAG = Constants.LOG_PREFIX + getClass().getSimpleName();

    private List<Job> jobs;

    public JobSearchAdapter(List<Job> jobs) {
        this.jobs = jobs;
    }

    public static class JobHolder extends RecyclerView.ViewHolder { //implements View.OnCreateContextMenuListener {

        int itemId;

        CardView searchCardView;
        TextView jobUserNameView;
        TextView jobNameView;
        TextView jobSearchResultId;

        JobHolder(View itemView) {
            super(itemView);
            searchCardView = (CardView) itemView.findViewById(R.id.job_search_cv);
            jobUserNameView = (TextView) itemView.findViewById(R.id.job_search_user_name);
            jobNameView = (TextView) itemView.findViewById(R.id.job_search_name);
            jobSearchResultId = (TextView) itemView.findViewById(R.id.job_search_result_id);

//            itemView.setOnCreateContextMenuListener(this);
        }

        public void setItemId(int id) {
            this.itemId = id;
        }


        //это скорее всего лишнее
//        @Override
//        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
//            menu.add(Constants.MENU_UPDATE, itemId, Menu.NONE, R.string.job_update);
//        }
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public JobHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_job_search_result, parent, false);
        JobHolder jobHolder = new JobHolder(v);
        return jobHolder;
    }

    @Override
    public void onBindViewHolder(final JobHolder holder, int position) {
        final Job item = this.jobs.get(position);

        holder.jobNameView.setText(item.getName());
        holder.jobUserNameView.setText(item.getUser().getName());
        holder.jobSearchResultId.setText(Integer.toString(item.getId()));

        holder.setItemId(item.getId());
    }

    @Override
    public int getItemCount() {
        return jobs.size();
    }


}
