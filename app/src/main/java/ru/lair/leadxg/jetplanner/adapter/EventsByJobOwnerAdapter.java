package ru.lair.leadxg.jetplanner.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import ru.lair.leadxg.jetplanner.R;
import ru.lair.leadxg.jetplanner.activity.EventControlActivity;
import ru.lair.leadxg.jetplanner.service.api.model.Event;
import ru.lair.leadxg.jetplanner.util.Constants;

public class EventsByJobOwnerAdapter extends BaseExpandableListAdapter {
    private final String TAG = Constants.LOG_PREFIX + getClass().getSimpleName();

    private ArrayList<String> mGroups;
    private HashMap<String,ArrayList<Event>> mChilds;
    private Context mContext;

    public EventsByJobOwnerAdapter(Context context, ArrayList<String> groups, HashMap<String, ArrayList<Event>> childs){
        mContext = context;
        mGroups = groups;
        mChilds = childs;
    }

    @Override
    public int getGroupCount() {
        return mGroups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mChilds.get(mGroups.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mGroups.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mChilds.get(mGroups.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.event_by_job_owner_group_item_layout, null);
        }

        if (isExpanded){
            //Изменяем что-нибудь, если текущая Group раскрыта
        }
        else{
            //Изменяем что-нибудь, если текущая Group скрыта
        }

        TextView textGroup = (TextView) convertView.findViewById(R.id.event_by_job_owner_group_item);
        TextView eventCountByGroup = (TextView) convertView.findViewById(R.id.event_count_by_group);
        textGroup.setText(mGroups.get(groupPosition));
        eventCountByGroup.setText(Integer.toString(getChildrenCount(groupPosition)));

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.event_by_job_owner_child_item_layout, null);
        }

        final TextView textChild = (TextView) convertView.findViewById(R.id.event_by_job_owner_child_item_name);
        final Event e = mChilds.get(mGroups.get(groupPosition)).get(childPosition);
        textChild.setText(e.toString());
        textChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.event_by_job_owner_child_item_name :
                        Intent intent = new Intent(mContext, EventControlActivity.class);
                        intent.putExtra("client_phone", e.getUser().getPhone());
                        intent.putExtra("client_name", e.getUser().getName());
                        intent.putExtra("job_id", e.getJobId());
                        intent.putExtra("event_id", e.getId());
                        intent.putExtra("job_name", e.getJob().getName());
                        intent.putExtra("desired_time", e.getDesiredTime());
                        intent.putExtra("event_status", e.getStatus());
                        mContext.startActivity(intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                        break;
                }
            }
        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}