package ru.lair.leadxg.jetplanner.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.lair.leadxg.jetplanner.R;
import ru.lair.leadxg.jetplanner.adapter.EventAdapter;
import ru.lair.leadxg.jetplanner.listener.recycler.RecyclerItemClickListener;
import ru.lair.leadxg.jetplanner.service.api.model.Event;
import ru.lair.leadxg.jetplanner.util.Constants;
import ru.lair.leadxg.jetplanner.util.PreferencesHelper;
import ru.lair.leadxg.jetplanner.util.RetroManager;

public class EventActivity extends AppCompatActivity implements View.OnClickListener {

    private final String TAG = Constants.LOG_PREFIX + getClass().getSimpleName();

    private RecyclerView recyclerView;

    private RetroManager retroManager;

    private PreferencesHelper preferencesHelper;
    private int userId;
    private String token;

    private int itemId = 0; // переменная содержит значение выбранного элемента при попытке его удаления
    private int jobId;

    ArrayList<Event> events = new ArrayList<>();
    EventAdapter eventAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        preferencesHelper = PreferencesHelper.getInstance(getSharedPreferences(Constants.USER_DATA_PREFERENCE, MODE_PRIVATE));
        token = preferencesHelper.getToken();
        userId = preferencesHelper.getUserId();

        recyclerView = (RecyclerView) findViewById(R.id.event_recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        retroManager = RetroManager.getInstance();
        eventAdapter = new EventAdapter(events);

        recyclerView.setAdapter(eventAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, onItemClickListener));
    }

    private RecyclerItemClickListener.OnItemClickListener onItemClickListener = new RecyclerItemClickListener.OnItemClickListener() {
        @Override
        public void onItemClick(View view, int position) {
//            updateJobAction(position);
            Toast.makeText(getApplicationContext(),"CLICK ON ITEM MENU", Toast.LENGTH_LONG).show();
        }
    };

    @Override
    public void onResume(){
        super.onResume();
        showAllEvents();
        Log.d(TAG, "eventLimitCounter: " + preferencesHelper.getEventCounter());
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        this.itemId = item.getItemId();
        switch (item.getGroupId()) {
            case Constants.MENU_UPDATE:
//                updateAddressAction(itemId);
                Toast.makeText(EventActivity.this, "Событие должно обновиться!", Toast.LENGTH_SHORT).show();
                break;
            case Constants.MENU_DELETE:
                showDialog(Constants.DIALOG_DELETE_CONFIRM);
                break;
            default:
                return super.onContextItemSelected(item);
        }
        return true;
    }

    public void showAllEvents() {
        Call<List<Event>> response = retroManager.getEventsByUserId(userId, token);
        response.enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
                List<Event> responseBody = response.body();
                Iterator<Event> iteratorToList = responseBody.iterator();
                events.clear();
                while (iteratorToList.hasNext()) {
                    Event event = iteratorToList.next();
                    if (event.getId() == 0){
                        return;
                    }
                    events.add(event);
                }
                //сортировка по eventId
                Collections.sort(events, COMPARE_BY_EVENTID);
                eventAdapter.notifyDataSetChanged();

                Log.d(TAG, "количество: " + events.size());
            }

            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }

    public static final Comparator<Event> COMPARE_BY_EVENTID = new Comparator<Event>() {
        @Override
        public int compare(Event event1, Event event2) {
            return event1.getId() - event2.getId();
        }
    };

    protected Dialog onCreateDialog(int id) {
        if (id == Constants.DIALOG_DELETE_CONFIRM) {
            AlertDialog.Builder adb = new AlertDialog.Builder(this);
            adb.setTitle(R.string.event_delete_dialog_title);
            adb.setMessage(R.string.remove_row_dialog_message);
            adb.setPositiveButton(R.string.delete_dialog_positive_btn, deleteUserDialog);
            adb.setNegativeButton(R.string.delete_dialog_negative_btn, deleteUserDialog);
            return adb.create();
        }
        return super.onCreateDialog(id);
    }

    DialogInterface.OnClickListener deleteUserDialog = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    deleteEvent(itemId);
                    break;
                case Dialog.BUTTON_NEGATIVE:
                    break;
            }
            itemId = 0;
        }
    };


    public void deleteEvent(int itemId) {
        try {
            Call<Event> response = retroManager.deleteEvent(itemId, userId, token);
            response.enqueue(new Callback<Event>() {
                @Override
                public void onResponse(Call<Event> call, Response<Event> response) {
                    preferencesHelper.decrementEventCounter();

                    Log.d(TAG, "eventLimitCounter: " + preferencesHelper.getEventCounter());
                    Toast.makeText(EventActivity.this, R.string.event_removed, Toast.LENGTH_SHORT).show();
                    showAllEvents();
                }

                @Override
                public void onFailure(Call<Event> call, Throwable t) {
                    Log.e(TAG, t.getMessage());
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
    }
}