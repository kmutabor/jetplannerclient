package ru.lair.leadxg.jetplanner.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import ru.lair.leadxg.jetplanner.R;
import ru.lair.leadxg.jetplanner.service.api.model.Job;
import ru.lair.leadxg.jetplanner.service.api.model.User;
import ru.lair.leadxg.jetplanner.util.Constants;
import ru.lair.leadxg.jetplanner.util.PreferencesHelper;
import ru.lair.leadxg.jetplanner.util.RetroManager;

public class MainActivity extends AppCompatActivity {

    private final String TAG = Constants.LOG_PREFIX + getClass().getSimpleName();

    private SearchView mSearchView;

    private RetroManager retroManager;
    private PreferencesHelper preferencesHelper;
    private int userId;
    private String token;

    private ArrayList<Job> jobs = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferencesHelper = PreferencesHelper.getInstance(getSharedPreferences(Constants.USER_DATA_PREFERENCE, MODE_PRIVATE));
//        preferencesHelper.saveUserIdAndToken(0, null); // для тестирования
//        preferencesHelper.saveUserJobCounter(0); //для принудительного обнуления счетчика
//        preferencesHelper.saveEventCounter(0); //для принудительного обнуления счетчика
        token = preferencesHelper.getToken();
        userId = preferencesHelper.getUserId();
        if (!isUserRegistered()) {
            Intent registration = new Intent(this, RegistrationActivity.class);
            startActivity(registration);
        } else {
            setContentView(R.layout.activity_main);

            mSearchView = (SearchView) findViewById(R.id.job_search_view);
            mSearchView.setQueryHint(getResources().getString(R.string.job_query_hint));

            retroManager = RetroManager.getInstance();

            //поиск
            mSearchView.setOnQueryTextListener(onQueryTextListener);

        }
    }

    @Override
    public void onResume() {
//        mSearchView.clearFocus(); // не нужно, так как это основная функция этой активности
        super.onResume();
    }

    //создание меню
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(Menu.NONE, Constants.MENU_JOB_ACTIVITY, Menu.NONE, R.string.title_activity_job);
        menu.add(Menu.NONE, Constants.MENU_EVENT_ACTIVITY, Menu.NONE, R.string.title_activity_event);
        menu.add(Menu.NONE, Constants.MENU_DELETE, Menu.NONE, R.string.delete_account);
        return super.onCreateOptionsMenu(menu);
    }

    //выбор элементов меню
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case Constants.MENU_JOB_ACTIVITY:
                Log.d(TAG, "Выбрано: " + item.getTitle().toString());
                Intent iJob = new Intent(this, JobActivity.class);
                startActivity(iJob);
                break;
            case Constants.MENU_EVENT_ACTIVITY:
                Log.d(TAG, "Выбрано: " + item.getTitle().toString());
                Intent iEvent = new Intent(this, EventActivity.class);
                startActivity(iEvent);
                break;
            case Constants.MENU_DELETE:
                Log.d(TAG, "Выбрано: " + item.getTitle().toString());
                showDialog(Constants.DIALOG_DELETE_CONFIRM);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Проверяет, на устройстве создан аккаунт или нет
     * @return
     */
    private boolean isUserRegistered() {
        Log.d(TAG, "isUserRegistered()");
        if (0 == userId || null == token) {
            Log.d(TAG, "USER NOT REGISTERED");
            return false;
        }
        Log.d(TAG, "USER ALREADY REGISTERED");
        return true;
    }

    /**
     * создает диалоговое оно удаления пользователя
     * @param id
     * @return
     */
    protected Dialog onCreateDialog(int id) {
        if (id == Constants.DIALOG_DELETE_CONFIRM) {
            AlertDialog.Builder adb = new AlertDialog.Builder(this);
            adb.setTitle(R.string.user_delete_dialog_title);
            adb.setMessage(R.string.user_delete_dialog_message);
//            adb.setIcon(android.R.drawable.ic_dialog_info);
            adb.setPositiveButton(R.string.delete_dialog_positive_btn, deleteUserDialog);
            adb.setNegativeButton(R.string.delete_dialog_negative_btn, deleteUserDialog);
            return adb.create();
        }
        return super.onCreateDialog(id);
    }

    /**
     * Слушает диалоговое окно удаления пользователя
     */
    DialogInterface.OnClickListener deleteUserDialog = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    deleteUserAccount();
                    Log.i(TAG, "USER DELETED");
                    break;
                case Dialog.BUTTON_NEGATIVE:
                    Log.i(TAG, "CANCEL USER DELETED");
                    break;
            }
        }
    };

    /**
     * Удаляет аккаунт пользователя и обнуляет все его данные для работы
     * после удаления запускает активность регистрации
     */
    private void deleteUserAccount() {
        RetroManager retroManager = RetroManager.getInstance();
        try {
            Call<User> response = retroManager.deleteUser(userId, token);
            response.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    User responseBody = (User) response.body();
                    if (null != responseBody) {
                        if ("deleted".equals(responseBody.getPhone())) {
                            /*
                             * Здесь должны очищатсья все пользовательские настройки
                             * не забывай добавлять, если что-то в preferences добавляется
                             */
                            preferencesHelper.saveUserIdAndToken(0, null);
                            preferencesHelper.saveUserJobCounter(0);
                            preferencesHelper.saveEventCounter(0);
                            Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                        }
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    Log.e(TAG, t.getMessage());
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Получает результат поиска и открывает ативность с результатом
     * @param jobName
     */
    public void searchForJobs(String jobName) {
        Call<List<Job>> response = retroManager.getJobsByName(userId, token, jobName);
        response.enqueue(new Callback<List<Job>>() {
            @Override
            public void onResponse(Call<List<Job>> call, Response<List<Job>> response) {

                List<Job> responseBody = response.body();
                Iterator<Job> iteratorToList = responseBody.iterator();
                jobs.clear();
                while (iteratorToList.hasNext()) {
                    jobs.add(iteratorToList.next());
                }
                if (checkSearchResult()) {
                    mSearchView.setQuery("", false);
                    Intent iFoundJobs = new Intent(getApplicationContext(), JobSearchResultActivity.class);
                    iFoundJobs.putExtra("jobList", jobs);
                    startActivity(iFoundJobs);
                }
            }

            @Override
            public void onFailure(Call<List<Job>> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }

    /**
     * Проверяет, есть ли результат поиска по названию джоба
     * @return true|false
     */
    private boolean checkSearchResult() {
        if (jobs.get(0).getId() == 0 || jobs.isEmpty()) {
            Toast.makeText(getApplicationContext(), R.string.err_not_found, Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    /**
     * Обработчик запроса поиска
     */
    private SearchView.OnQueryTextListener onQueryTextListener =  new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String s) {
            searchForJobs(s);
            return true;
        }

        @Override
        public boolean onQueryTextChange(String s) {
            return false;
        }
    };

}


