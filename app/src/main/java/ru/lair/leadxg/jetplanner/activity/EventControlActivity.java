package ru.lair.leadxg.jetplanner.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.lair.leadxg.jetplanner.R;
import ru.lair.leadxg.jetplanner.adapter.DialogArrayAdapter;
import ru.lair.leadxg.jetplanner.service.api.model.Event;
import ru.lair.leadxg.jetplanner.util.Constants;
import ru.lair.leadxg.jetplanner.util.PreferencesHelper;
import ru.lair.leadxg.jetplanner.util.RetroManager;

public class EventControlActivity extends AppCompatActivity implements View.OnClickListener {
    private final String TAG = Constants.LOG_PREFIX + getClass().getSimpleName();

    private PreferencesHelper preferencesHelper;
    private String token;
    private int userId;
    private String clientName;
    private String clientPhone;
    private String jobName;
    private int jobId;
    private int eventId;
    private String desiredTime;
    private String eventStatus;
    private RetroManager retroManager;

    private TextView mClientName;
    private TextView mClientPhone;
    private TextView mJobName;
    private TextView mDesiredTime;
    private TextView mEventStatus;
    private Button mEventStatusChangeBtn;
    private Button mPhoneBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_control);

        Intent jobsIntent = getIntent();
        clientName = jobsIntent.getStringExtra("client_name");
        clientPhone = jobsIntent.getStringExtra("client_phone");
        jobName = jobsIntent.getStringExtra("job_name");
        jobId = jobsIntent.getIntExtra("job_id", 0);
        eventId = jobsIntent.getIntExtra("event_id", 0);
        desiredTime = jobsIntent.getStringExtra("desired_time");
        eventStatus = jobsIntent.getStringExtra("event_status");

        preferencesHelper = PreferencesHelper.getInstance(getSharedPreferences(Constants.USER_DATA_PREFERENCE, MODE_PRIVATE));
        token = preferencesHelper.getToken();
        userId = preferencesHelper.getUserId();
        retroManager = RetroManager.getInstance();

        mClientName = (TextView) findViewById(R.id.clientControl);
        mClientPhone = (TextView) findViewById(R.id.clientPhoneControl);
        mJobName = (TextView) findViewById(R.id.jobNameControl);
        mDesiredTime = (TextView) findViewById(R.id.eventDesiredTimeControl);
        mEventStatus = (TextView) findViewById(R.id.eventStatusControl);
        mEventStatusChangeBtn = (Button) findViewById(R.id.changeStatusBtn);
        mPhoneBtn = (Button) findViewById(R.id.phoneBtn);

        mPhoneBtn.setOnClickListener(this);
        mEventStatusChangeBtn.setOnClickListener(this);

        mClientName.setText(clientName);
        mClientPhone.setText(clientPhone);
        mJobName.setText(jobName);
        mDesiredTime.setText(desiredTime);
        mEventStatus.setText(eventStatus);
    }

    @Override
    public void onResume() {
        setStatusColorAndText(eventStatus);
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.changeStatusBtn:
                showDialog();
                break;
            case R.id.phoneBtn:
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + clientPhone));
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
        }
    }

    private void updateStatus(final int jobId, final int eventId, final String status, final AlertDialog alertDialog){
        if(eventStatus.equalsIgnoreCase(status)){
            return;
        }
        Call<Event> response = retroManager.updateStatus(userId, token, jobId, eventId, status);
        response.enqueue(new Callback<Event>() {
            @Override
            public void onResponse(Call<Event> call, Response<Event> response) {
                Event event = response.body();
                eventStatus = event.getStatus();
                setStatusColorAndText(eventStatus);
                alertDialog.dismiss();
                Toast.makeText(EventControlActivity.this, R.string.event_status_changed, Toast.LENGTH_LONG).show();
                Log.d(TAG, ("Изменен статус: " + eventId + " : " + event.getStatus()));
            }

            @Override
            public void onFailure(Call<Event> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }

    public void showDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(EventControlActivity.this);
        LayoutInflater inflater = EventControlActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_listview, null);
        dialogBuilder.setView(dialogView);

        final ListView listView = (ListView) dialogView.findViewById(R.id.listview);

        String[] values;
        if(eventStatus.equalsIgnoreCase(Constants.EventStatus.REQUEST)){
            values = new String[] {
                    getResources().getString(R.string.status_dialog_approved_btn),
                    getResources().getString(R.string.status_dialog_cancelled_btn),
            };
        }else{
            values = new String[] {
                    getResources().getString(R.string.status_dialog_done_btn),
                    getResources().getString(R.string.status_dialog_cancelled_btn),
            };
        }

        final AlertDialog alertDialog = dialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        alertDialog.show();

        DialogArrayAdapter<String> adapter = new DialogArrayAdapter<String>(this, R.layout.dialog_item, values, eventStatus);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                switch (position) {
                    case 0:
                        if(eventStatus.equalsIgnoreCase(Constants.EventStatus.REQUEST)){
                            updateStatus(jobId, eventId, Constants.EventStatus.APPROVED, alertDialog);
                        }else{
                            updateStatus(jobId, eventId, Constants.EventStatus.DONE, alertDialog);
                        }
                        break;
                    case 1:
                        updateStatus(jobId, eventId, Constants.EventStatus.CANCELLED, alertDialog);
                        break;
                }

            }
        });
    }


    public void setStatusColorAndText(String status) {
        mEventStatus.setText(status);
        switch (status) {
            case Constants.EventStatus.REQUEST:
                mEventStatus.setTextColor(ContextCompat.getColor(this, R.color.statusRequest));
                break;
            case Constants.EventStatus.APPROVED:
                mEventStatus.setTextColor(ContextCompat.getColor(this, R.color.statusApproved));
                break;
            case Constants.EventStatus.CANCELLED:
                mEventStatus.setTextColor(ContextCompat.getColor(this, R.color.statusCanсelled));
                break;
            case Constants.EventStatus.EXPIRED:
                mEventStatus.setTextColor(ContextCompat.getColor(this, R.color.statusExpired));
                break;
            case Constants.EventStatus.DONE:
                mEventStatus.setTextColor(ContextCompat.getColor(this, R.color.statusDone));
                break;
        }
    }
}
