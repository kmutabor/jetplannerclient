package ru.lair.leadxg.jetplanner.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import ru.lair.leadxg.jetplanner.R;
import ru.lair.leadxg.jetplanner.util.Constants;

public class DialogArrayAdapter<String> extends ArrayAdapter<String>{
    Context context;
    int resource;
    String[] statuses;
    java.lang.String eventStatus;

    public DialogArrayAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull String[] objects, java.lang.String eventStatus) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.statuses = objects;
        this.eventStatus = eventStatus;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            v =  View.inflate(context, resource, null);
            LinearLayout mDialogTv = (LinearLayout) v.findViewById(R.id.event_change_status_dialog_linear_layout);
            TextView tv = (TextView) mDialogTv.getChildAt(0);
            CharSequence statusText = (CharSequence)statuses[position];
            int bgColor = 0;
            switch (position) {
                case 0:
                    if(Constants.EventStatus.REQUEST.equalsIgnoreCase(eventStatus)){
                        bgColor = context.getResources().getColor(R.color.statusApproved);
                    }else{
                        bgColor = context.getResources().getColor(R.color.statusDone);
                    }
                    break;
                case 1:
                    bgColor = context.getResources().getColor(R.color.statusCanсelled);
                    break;
            }
            mDialogTv.setBackgroundColor(bgColor);
            tv.setText(statusText);
        }
        return v;
    }
}
