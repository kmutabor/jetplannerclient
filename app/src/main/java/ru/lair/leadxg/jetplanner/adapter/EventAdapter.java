package ru.lair.leadxg.jetplanner.adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ru.lair.leadxg.jetplanner.R;
import ru.lair.leadxg.jetplanner.service.api.model.Event;
import ru.lair.leadxg.jetplanner.util.Constants;


public class EventAdapter extends RecyclerView.Adapter<EventAdapter.EventHolder> {

    List<Event> events;
    String modifiedTime;
    String status;

    public EventAdapter(List<Event> events) {
        this.events = events;
    }

    public static class EventHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        int itemId;

        CardView cv;
        TextView mEventIdView;
        TextView mEventJobNameView;
        TextView mEventDesiredTimeView;
        TextView mEventJobOwnerView;
        TextView mEventStatusView;

        EventHolder(View itemView) {
            super(itemView);

            cv = (CardView) itemView.findViewById(R.id.event_cv);
            mEventIdView = (TextView) itemView.findViewById(R.id.event_id_view);
            mEventJobNameView = (TextView) itemView.findViewById(R.id.event_job_name_view);
            mEventDesiredTimeView = (TextView) itemView.findViewById(R.id.event_desired_time_view);
            mEventJobOwnerView = (TextView) itemView.findViewById(R.id.event_job_owner_view);
            mEventStatusView = (TextView) itemView.findViewById(R.id.event_status_view);
            itemView.setOnCreateContextMenuListener(this);
        }

        public void setItemId(int id) {
            this.itemId = id;
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.add(Constants.MENU_UPDATE, itemId, Menu.NONE, R.string.event_update);
            menu.add(Constants.MENU_DELETE, itemId, Menu.NONE, R.string.event_remove);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public EventHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_event, parent, false);
        EventHolder eventHolder = new EventHolder(v);
        return eventHolder;
    }

    @Override
    public void onBindViewHolder(final EventHolder holder, int position) {
        final Event item = this.events.get(position);
        modifiedTime = item.getJobId() == 0 ? item.getDesiredTime() : item.getDesiredTime().substring(0, item.getDesiredTime().lastIndexOf(":"));
        status = item.getStatus();

        holder.mEventIdView.setText(Integer.toString(item.getId()));
        holder.mEventJobNameView.setText(item.getJob().getName());

        //вывод имени создателя
        holder.mEventJobOwnerView.setText(item.getUser().getName());
        holder.mEventDesiredTimeView.setText(modifiedTime);

        switch (status) {
            case "request":
                holder.mEventStatusView.setTextColor(ContextCompat.getColor(holder.mEventStatusView.getContext(), R.color.statusRequest));
                break;
            case "approved":
                holder.mEventStatusView.setTextColor(ContextCompat.getColor(holder.mEventStatusView.getContext(), R.color.statusApproved));
                break;
            case "cancelled":
                holder.mEventStatusView.setTextColor(ContextCompat.getColor(holder.mEventStatusView.getContext(), R.color.statusCanсelled));
                break;
            case "expired":
                holder.mEventStatusView.setTextColor(ContextCompat.getColor(holder.mEventStatusView.getContext(), R.color.statusExpired));
                break;
            case "done":
                holder.mEventStatusView.setTextColor(ContextCompat.getColor(holder.mEventStatusView.getContext(), R.color.statusDone));
                break;
        }
        holder.mEventStatusView.setText(status);
        holder.setItemId(item.getId());
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

}
