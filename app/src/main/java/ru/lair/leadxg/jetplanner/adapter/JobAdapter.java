package ru.lair.leadxg.jetplanner.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ru.lair.leadxg.jetplanner.R;
import ru.lair.leadxg.jetplanner.service.api.model.Job;
import ru.lair.leadxg.jetplanner.util.Constants;

public class JobAdapter extends RecyclerView.Adapter<JobAdapter.JobHolder> {

    protected final String TAG = Constants.LOG_PREFIX + getClass().getSimpleName();

    List<Job> jobs;

    public JobAdapter(List<Job> jobs) {
        this.jobs = jobs;
    }

    public static class JobHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        int itemId;

        CardView cv;
        TextView mJobIdView;
        TextView mJobNameView;
        TextView mJobAddressView;
        TextView mJobDescView;
        TextView mJobEventCountView;

        JobHolder(View itemView) {
            super(itemView);

            cv = (CardView) itemView.findViewById(R.id.job_cv);
            mJobIdView = (TextView) itemView.findViewById(R.id.job_id_view);
            mJobNameView = (TextView) itemView.findViewById(R.id.job_name_view);
            mJobAddressView = (TextView) itemView.findViewById(R.id.job_address_view);
            mJobDescView = (TextView) itemView.findViewById(R.id.job_desc_view);
            mJobEventCountView = (TextView) itemView.findViewById(R.id.job_event_count_view);
            itemView.setOnCreateContextMenuListener(this);
        }

        public void setItemId(int id) {
            this.itemId = id;
        }


        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.add(Constants.MENU_UPDATE, itemId, Menu.NONE, R.string.job_update);
            menu.add(Constants.MENU_DELETE, itemId, Menu.NONE, R.string.job_remove);
        }
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public JobHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_job, parent, false);
        JobHolder jobHolder = new JobHolder(v);
        return jobHolder;
    }

    @Override
    public void onBindViewHolder(final JobHolder holder, int position) {
        final Job item = this.jobs.get(position);
        holder.mJobIdView.setText(Integer.toString(item.getId()));
        holder.mJobNameView.setText(item.getName());
        holder.mJobAddressView.setText(item.getAddress().getAddress());
        holder.mJobDescView.setText(item.getDescription());
        holder.mJobEventCountView.setText(Integer.toString(item.getEventCount()));

        holder.setItemId(item.getId());
    }

    @Override
    public int getItemCount() {
        return jobs.size();
    }

}
