package ru.lair.leadxg.jetplanner.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.lair.leadxg.jetplanner.R;
import ru.lair.leadxg.jetplanner.adapter.EventsByJobOwnerAdapter;
import ru.lair.leadxg.jetplanner.service.api.model.Event;
import ru.lair.leadxg.jetplanner.util.Constants;
import ru.lair.leadxg.jetplanner.util.PreferencesHelper;
import ru.lair.leadxg.jetplanner.util.RetroManager;


public class EventListByJobOwnerActivity extends AppCompatActivity {
    private final String TAG = Constants.LOG_PREFIX + getClass().getSimpleName();

    private PreferencesHelper preferencesHelper;
    private String token;
    private int userId;
    private RetroManager retroManager;
    private EventsByJobOwnerAdapter adapter;

    private int jobId = 0;

    private ExpandableListView expandableListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events_by_job_owner);

        preferencesHelper = PreferencesHelper.getInstance(getSharedPreferences(Constants.USER_DATA_PREFERENCE, MODE_PRIVATE));
        token = preferencesHelper.getToken();
        userId = preferencesHelper.getUserId();
        retroManager = RetroManager.getInstance();

        Intent jobsIntent = getIntent();
        jobId = jobsIntent.getIntExtra("job_id", 0);
    }

    @Override
    public void onResume() {
        showData(jobId);
        super.onResume();
    }


    private void fillExpandableView(List<Event> events){
        if (events.size() == 0){
            return;
        }

        ArrayList<String> groups = new ArrayList<>();
        for (int i = 0; i < events.size(); i++) {
            String desiredTime = String.copyValueOf(events.get(i).getDesiredTime().toCharArray(), 0, 10);
            if(!groups.contains(desiredTime)){
                groups.add(desiredTime);
            }
        }
        // Эта сортровка пока работает, но неизвестно, что будет если будет больше дат.
        Collections.sort(groups, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });

        HashMap<String, ArrayList<Event>> childs = new HashMap<>();
        for (int i = 0; i < events.size(); i++) {
            String desiredTime = String.copyValueOf(events.get(i).getDesiredTime().toCharArray(), 0, 10);
            if(!childs.containsKey(desiredTime)){
                childs.put(desiredTime, new ArrayList<Event>());
            }
            childs.get(desiredTime).add(events.get(i));
        }


        adapter = new EventsByJobOwnerAdapter(getApplicationContext(), groups, childs);
        expandableListView = (ExpandableListView)findViewById(R.id.event_by_job_owner_expandable_list_view);
        expandableListView.setAdapter(adapter);

        //скорее всего не нужно
//        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
//            @Override
//            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {
//                //Log.d(TAG, expandableListView.getItemAtPosition(i1) + "");
//
//                return false;
//            }
//        });
    }


    private void showData(int jobId){
        Call<List<Event>> response = retroManager.getEventsByJobId(userId, token, jobId);
        response.enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
                List<Event> responseBody = response.body();
                Iterator<Event> iteratorToList = responseBody.iterator();
                List<Event> list = new ArrayList<Event>();
                while (iteratorToList.hasNext()) {
                    Event adr = iteratorToList.next();
                    list.add(adr);
                }
                fillExpandableView(list);
            }

            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

}
