package ru.lair.leadxg.jetplanner.service.api;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ru.lair.leadxg.jetplanner.service.api.model.Address;
import ru.lair.leadxg.jetplanner.service.api.model.Event;
import ru.lair.leadxg.jetplanner.service.api.model.Job;
import ru.lair.leadxg.jetplanner.service.api.model.User;
import ru.lair.leadxg.jetplanner.util.Constants;


public interface JetPlannerWebApi {
    /*
    * *************************************************************************************************
    * ПОЛЬЗОВАТЕЛИ
    */

    /**
     * @param user_id
     * @param token
     * @return
     */
    @GET(Constants.Request.USER)
    Call<List<User>> getUsers(@Query("user_id") int user_id, @Query("token") String token);

    /**
     * @param user_id
     * @param token
     * @return
     */
    @DELETE(Constants.Request.USER + "/{user_id}/{token}")
    Call<User> deleteUser(@Path("user_id") int user_id, @Path("token") String token);




    /*
     * *************************************************************************************************
     * РЕГИСТРАЦИЯ
     */

    /**
     * @param phone
     * @return
     */
    @FormUrlEncoded
    @POST(Constants.Request.USER)
    Call<User> registration(@Field("phone") String phone, @Field("name") String name);



    /*
     * ****************************************************************************************************
     * АДРЕСА
     */


    /**
     * @param user_id
     * @return
     */
    @GET(Constants.Request.ADDRESS)
    Call<Address> getAddressById(@Query("user_id") int user_id, @Query("token") String token, @Query("address_id") int address_id);


    /**
     * @param address_id
     * @param user_id
     * @param token
     * @return
     */
    @DELETE(Constants.Request.ADDRESS + "/{address_id}/{user_id}/{token}")
    Call<Address> deleteAddress(@Path("address_id") int address_id, @Path("user_id") int user_id, @Path("token") String token);


    /**
     *
     * @param user_id
     * @param token
     * @param addressId
     * @param address
     * @param latitude
     * @param longitude
     * @return
     */
    @FormUrlEncoded
    @PUT(Constants.Request.ADDRESS)
    Call<Address> updateAddress(@Field("user_id") int user_id, @Field("token") String token, @Field("address_id") int addressId, @Field("address") String address, @Field("latitude") double latitude, @Field("longitude") double longitude);




    /**
     *
     * @param user_id
     * @param token
     * @param address
     * @param latitude
     * @param longitude
     * @return
     */
    @FormUrlEncoded
    @POST(Constants.Request.ADDRESS)
    Call<Address> createAddress(@Field("user_id") int user_id, @Field("token") String token, @Field("address") String address, @Field("latitude") double latitude, @Field("longitude") double longitude);




    /*
     * ****************************************************************************************************
     * УСЛУГИ
     */


    /**
     *
     * @param user_id
     * @param token
     * @param addressId
     * @param name
     * @param description
     * @param timeInteval
     * @param workHourStart
     * @param workHourEnd
     * @param amount
     * @return
     */
    @FormUrlEncoded
    @POST(Constants.Request.JOB)
    Call<Job> addJob(
            @Field("user_id") int user_id,
            @Field("token") String token,
            @Field("address_id") int addressId,
            @Field(value = "name") String name,
            @Field("description") String description,
            @Field("time_interval") int timeInteval,
            @Field("work_hour_start") int workHourStart,
            @Field("work_hour_end") int workHourEnd,
            @Field("amount") int amount
            );


    /**
     * @param user_id
     * @param token
     * @param owner_id
     * @return
     */
    @GET(Constants.Request.JOB)
    Call<List<Job>> getJobsByOwnerId(@Query("user_id") int user_id, @Query("token") String token, @Query("owner_id") int owner_id);

    @GET(Constants.Request.JOB)
    Call<List<Job>> getJobsByUserId(@Query("user_id") int user_id, @Query("token") String token);

    @GET(Constants.Request.JOB)
    Call<List<Job>> getJobsByName(@Query("user_id") int user_id, @Query("token") String token, @Query("name") String name);

    @GET(Constants.Request.JOB)
    Call<List<Job>> getJobsByDescription(@Query("user_id") int user_id, @Query("token") String token, @Query("description") String description);

    /**
     *
     * @param userId
     * @param token
     * @param jobId
     * @param addressId
     * @param name
     * @param description
     * @param timeInterval
     * @param workHourStart
     * @param workHourEnd
     * @param amount
     * @return
     */
    @FormUrlEncoded
    @PUT(Constants.Request.JOB)
    Call<Job> updateJob(
            @Field("user_id") int userId,
            @Field("token") String token,
            @Field("job_id") int jobId,
            @Field("address_id") int addressId,
            @Field("name") String name,
            @Field("description") String description,
            @Field("time_interval") int timeInterval,
            @Field("work_hour_start") int workHourStart,
            @Field("work_hour_end") int workHourEnd,
            @Field("amount") int amount
    );



    /**
     * @param job_id
     * @param user_id
     * @param token
     * @return
     */
    @DELETE(Constants.Request.JOB + "/{job_id}/{user_id}/{token}")
    Call<Job> deleteJob(@Path("job_id") int job_id, @Path("user_id") int user_id, @Path("token") String token);




    /*
     * ****************************************************************************************************
     * СОБЫТИЯ
     */

    /**
     *
     * @param user_id
     * @param token
     * @param job_id
     * @param status
     * @param desiredTime
     * @return
     */
    @FormUrlEncoded
    @POST(Constants.Request.EVENT)
    Call<Event> addEvent(@Field("user_id") int user_id, @Field("token") String token, @Field("job_id") int job_id, @Field(value = "status") String status, @Field("desired_time") String desiredTime);

    @GET(Constants.Request.EVENT)
    Call<List<Event>> getEventsByUserId(@Query("user_id") int user_id, @Query("token") String token);

    @GET(Constants.Request.EVENT)
    Call<List<Event>> getEventsByJobId(@Query("user_id") int user_id, @Query("token") String token, @Query("job_id") int job_id);

    @GET(Constants.Request.EVENT)
    Call<List<Event>> getEventsByJobIdForClient(@Query("user_id") int user_id, @Query("token") String token, @Query("job_id") int job_id, @Query("client") boolean client);

    /**
     *
     * @param userId
     * @param token
     * @param jobId
     * @param eventId
     * @param status
     * @return
     */
    @FormUrlEncoded
    @PUT(Constants.Request.EVENT)
    Call<Event> updateStatus(
            @Field("user_id") int userId,
            @Field("token") String token,
            @Field("job_id") int jobId,
            @Field("event_id") int eventId,
            @Field("event_status") String status
    );

    /**
     *
     * @param event_id
     * @param user_id
     * @param token
     * @return
     */
    @DELETE(Constants.Request.EVENT + "/{event_id}/{user_id}/{token}")
    Call<Event> deleteEvent(@Path("event_id") int event_id, @Path("user_id") int user_id, @Path("token") String token);

}
