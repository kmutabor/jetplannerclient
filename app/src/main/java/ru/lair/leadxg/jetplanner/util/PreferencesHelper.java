package ru.lair.leadxg.jetplanner.util;


import android.content.SharedPreferences;

public class PreferencesHelper {

    private static PreferencesHelper preferencesHelper;

    private static SharedPreferences sharedPreferences;

    private PreferencesHelper(SharedPreferences sP) {
        sharedPreferences = sP;
    }

    public static PreferencesHelper getInstance(SharedPreferences sharedPreferences) {
        if (null == preferencesHelper) {
            preferencesHelper = new PreferencesHelper(sharedPreferences);
        }
        return preferencesHelper;
    }


    public int getUserId() {
        return sharedPreferences.getInt(Constants.USER_ID, 0);
    }

    public String getToken() {
        return sharedPreferences.getString(Constants.USER_AUTH_TOKEN, null);
    }

    public Integer getJobCounter() {
        return sharedPreferences.getInt(Constants.USER_JOB_COUNT, 0);
    }

    public Integer getAddressCounter() {
        return sharedPreferences.getInt(Constants.ADDRESS_COUNT, 0);
    }

    public Integer getEventCounter() {
        return sharedPreferences.getInt(Constants.EVENT_COUNT, 0);
    }

    public void saveUserIdAndToken(int userId, String token) {
        SharedPreferences.Editor ed = sharedPreferences.edit();
        ed.putInt(Constants.USER_ID, userId);
        ed.putString(Constants.USER_AUTH_TOKEN, token);
        ed.commit();
    }

    public void saveUserJobCounter(int jobCounter) {
        SharedPreferences.Editor ed = sharedPreferences.edit();
        ed.putInt(Constants.USER_JOB_COUNT, jobCounter);
        ed.commit();
    }


    public void saveEventCounter(int eventCounter) {
        SharedPreferences.Editor ed = sharedPreferences.edit();
        ed.putInt(Constants.EVENT_COUNT, eventCounter);
        ed.commit();
    }

    public void incrementUserJobCounter() {
        int counter = this.getJobCounter();
        this.saveUserJobCounter(++counter);
    }

    public void decrementUserJobCounter() {
        int counter = this.getJobCounter();
        if(counter > 0){
            this.saveUserJobCounter(--counter);
        }
    }

    public void incrementEventCounter() {
        int counter = this.getEventCounter();
        this.saveEventCounter(++counter);
    }

    public void decrementEventCounter() {
        int counter = this.getEventCounter();
        if(counter > 0){
            this.saveEventCounter(--counter);
        }
    }
}
