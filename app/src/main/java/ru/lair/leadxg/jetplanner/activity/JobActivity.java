package ru.lair.leadxg.jetplanner.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import ru.lair.leadxg.jetplanner.R;
import ru.lair.leadxg.jetplanner.adapter.JobAdapter;
import ru.lair.leadxg.jetplanner.listener.recycler.RecyclerItemClickListener;
import ru.lair.leadxg.jetplanner.service.api.model.Job;
import ru.lair.leadxg.jetplanner.util.Constants;
import ru.lair.leadxg.jetplanner.util.PreferencesHelper;
import ru.lair.leadxg.jetplanner.util.RetroManager;

public class JobActivity extends AppCompatActivity implements View.OnClickListener {

    private final String TAG = Constants.LOG_PREFIX + getClass().getSimpleName();

    private RecyclerView recyclerView;
    private Button createJobButton;

    private RetroManager retroManager;

    private PreferencesHelper preferencesHelper;
    private int userId;
    private String token;

    private int itemId = 0; // переменная содержит значение выбранного элемента при попытке его удаления

    ArrayList<Job> jobs = new ArrayList<>();
    JobAdapter jobAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job);

        preferencesHelper = PreferencesHelper.getInstance(getSharedPreferences(Constants.USER_DATA_PREFERENCE, MODE_PRIVATE));
        token = preferencesHelper.getToken();
        userId = preferencesHelper.getUserId();

        createJobButton = (Button) findViewById(R.id.job_create_btn);

        recyclerView = (RecyclerView) findViewById(R.id.job_recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        createJobButton.setOnClickListener(this);

        retroManager = RetroManager.getInstance();
        jobAdapter = new JobAdapter(jobs);

        recyclerView.setAdapter(jobAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, onItemClickListener));
    }

    private RecyclerItemClickListener.OnItemClickListener onItemClickListener = new RecyclerItemClickListener.OnItemClickListener() {
        @Override
        public void onItemClick(View view, int position) {
            Job job = jobs.get(position);
            if(job.getEventCount() > 0){
                Intent intent = new Intent(getApplicationContext(), EventListByJobOwnerActivity.class);
                intent.putExtra("job_id", job.getId());
                startActivity(intent);
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        showAllJobs();
        Log.d(TAG, "jobLimitCounter: " + preferencesHelper.getJobCounter());
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        this.itemId = item.getItemId();
        switch (item.getGroupId()) {
            case Constants.MENU_UPDATE:
                updateJobAction(itemId);
                break;
            case Constants.MENU_DELETE:
                showDialog(Constants.DIALOG_DELETE_CONFIRM);
                break;
            default:
                return super.onContextItemSelected(item);
        }
        return true;
    }

    protected Dialog onCreateDialog(int id) {
        if (id == Constants.DIALOG_DELETE_CONFIRM) {
            AlertDialog.Builder adb = new AlertDialog.Builder(this);
            adb.setTitle(R.string.job_remove_dialog_title);
            adb.setMessage(R.string.remove_row_dialog_message);
            adb.setPositiveButton(R.string.delete_dialog_positive_btn, deleteUserDialog);
            adb.setNegativeButton(R.string.delete_dialog_negative_btn, deleteUserDialog);
            return adb.create();
        }
        return super.onCreateDialog(id);
    }

    DialogInterface.OnClickListener deleteUserDialog = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    deleteJob(itemId);
                    break;
                case Dialog.BUTTON_NEGATIVE:
                    break;
            }
            itemId = 0;
        }
    };


    public void showAllJobs() {
        Call<List<Job>> response = retroManager.getJobsByUserId(userId, token);
        response.enqueue(new Callback<List<Job>>() {
            @Override
            public void onResponse(Call<List<Job>> call, Response<List<Job>> response) {
                List<Job> responseBody = response.body();
                Iterator<Job> iteratorToList = responseBody.iterator();
                jobs.clear();
                while (iteratorToList.hasNext()) {
                    Job job = iteratorToList.next();
                    if(job.getId() == 0){
                        break;
                    }
                    jobs.add(job);
                }

                //сортировка по jobId
                Collections.sort(jobs, COMPARE_BY_JOBID);
                jobAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Job>> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }

    //объявление сортировки
    public static final Comparator<Job> COMPARE_BY_JOBID = new Comparator<Job>() {
        @Override
        public int compare(Job job1, Job job2) {
            return job2.getEventCount() - job1.getEventCount();
        }
    };

    private void updateJobAction(int jobId){
        Job job = null;
        for (Job j : jobs) {
            if(j.getId() == jobId){
                job = j;
                break;
            }
        }
        if (job != null){
            Log.e(TAG, job.getAddress().toString());
            Intent jobIntent = new Intent(JobActivity.this, JobCreateActivity.class);
            jobIntent.putExtra("update", true);
            jobIntent.putExtra("job_id", job.getId());
            jobIntent.putExtra("name", job.getName());
            jobIntent.putExtra("description", job.getDescription());
            jobIntent.putExtra("time_interval", job.getTimeInterval());
            jobIntent.putExtra("work_hour_start", job.getWorkHourStart());
            jobIntent.putExtra("work_hour_end", job.getWorkHourEnd());
            jobIntent.putExtra("amount", job.getAmount());

            jobIntent.putExtra("address_id", job.getAddressId());
            jobIntent.putExtra("address_name", job.getAddress().getAddress());
            double latitude = job.getAddress().getLatitude();
            jobIntent.putExtra("latitude", latitude);
            double longitude = job.getAddress().getLongitude();
            jobIntent.putExtra("longitude", longitude);
            startActivity(jobIntent);
        }

    }

    private void deleteJob(int itemId) {
        try {
            Call<Job> response = retroManager.deleteJob(itemId, userId, token);
            response.enqueue(new Callback<Job>() {
                @Override
                public void onResponse(Call<Job> call, Response<Job> response) {
                    preferencesHelper.decrementUserJobCounter();

                    Log.d(TAG, "jobLimitCounter: " + preferencesHelper.getJobCounter());
                    Toast.makeText(JobActivity.this, R.string.job_removed, Toast.LENGTH_SHORT).show();
                    showAllJobs();
                }

                @Override
                public void onFailure(Call<Job> call, Throwable t) {
                    Log.e(TAG, t.getMessage());
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.job_create_btn:
                Intent iCreateJob = new Intent(this, JobCreateActivity.class);
                startActivity(iCreateJob);
                break;
        }
    }
}
