package ru.lair.leadxg.jetplanner.activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.lair.leadxg.jetplanner.R;
import ru.lair.leadxg.jetplanner.service.api.model.Address;
import ru.lair.leadxg.jetplanner.service.api.model.Job;
import ru.lair.leadxg.jetplanner.util.Constants;
import ru.lair.leadxg.jetplanner.util.PreferencesHelper;
import ru.lair.leadxg.jetplanner.util.RetroManager;

public class JobCreateActivity extends AppCompatActivity implements View.OnClickListener {

    private final String TAG = Constants.LOG_PREFIX + getClass().getSimpleName();

    private List<Address> addressList = new ArrayList<Address>();;
    private final ArrayList<String> timeIntervalList = new ArrayList<String>();

    private EditText jobNameInputField;
    private EditText jobDescInputField;
    private EditText jobAmountInputField;
    private Button jobSubmitBtn;
    private TextView jobAddAddressTextView;
    private Spinner selectTimeIntervalSpinner;
    private Spinner workHourStartSpinner;
    private Spinner workHourEndSpinner;

    private int userId;
    private String token;

    private PreferencesHelper preferencesHelper;

    private RetroManager retroManager;

    private int jobId;

    private int addressId;
    private String addressName;
    private double latitude;
    private double longitude;

    private String jobNameValue;
    private String jobDescriptionValue;
    private int jobAmountValue;
    private int timeInterval = Constants.TimeInterval._60;
    private int workHourStartValue = Constants.WorkHour.START;
    private int workHourEndValue = Constants.WorkHour.END;

    private boolean updateFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_job);

        retroManager = RetroManager.getInstance();
        preferencesHelper = PreferencesHelper.getInstance(getSharedPreferences(Constants.USER_DATA_PREFERENCE, MODE_PRIVATE));
        userId = preferencesHelper.getUserId();
        token = preferencesHelper.getToken();

        jobNameInputField = (EditText) findViewById(R.id.job_name_create_et);
        jobDescInputField = (EditText) findViewById(R.id.job_description_create_et);

        jobAmountInputField = (EditText) findViewById(R.id.job_amount_create_et);
//        jobAmountInputField.setText(jobAmountValue);

        jobSubmitBtn = (Button) findViewById(R.id.job_create_submit_btn);
        jobAddAddressTextView = (TextView) findViewById(R.id.job_address_create_update_tv);
        jobAddAddressTextView.setText(getResources().getText(R.string.job_address_create_btn));

        initTimeIntervalSpinner();

        jobSubmitBtn.setOnClickListener(this);
        jobAddAddressTextView.setOnClickListener(this);

        initWorkHourSpinners();

        Intent jobIntent = getIntent();

        // если пришли данные для обновления заполняются значения полей
        if(jobIntent.hasExtra("update")){
            updateFlag = true;

            jobId = jobIntent.getIntExtra("job_id", 0);
            addressId = jobIntent.getIntExtra("address_id", 0);
            latitude = jobIntent.getDoubleExtra("latitude", 0);
            longitude = jobIntent.getDoubleExtra("longitude", 0);

            addressName = jobIntent.getStringExtra("address_name");
            jobAddAddressTextView.setText(addressName);

            jobNameValue = jobIntent.getStringExtra("name");
            jobDescriptionValue = jobIntent.getStringExtra("description");
            timeInterval = jobIntent.getIntExtra("time_interval", Constants.TimeInterval._60);

            workHourStartValue = jobIntent.getIntExtra("work_hour_start", Constants.WorkHour.START);
            workHourEndValue = jobIntent.getIntExtra("work_hour_end", Constants.WorkHour.END);

            jobNameInputField.setText(jobNameValue);

            jobDescriptionValue = jobIntent.getStringExtra("description");
            jobDescInputField.setText(jobDescriptionValue);

            jobAmountValue = jobIntent.getIntExtra("amount", 100);
            jobAmountInputField.setText(String.valueOf(jobAmountValue));

            timeInterval = jobIntent.getIntExtra("time_interval",Constants.TimeInterval._60);
            setSelectedValueTimeIntervalSpinner(timeInterval);
            selectTimeIntervalSpinner.setEnabled(false);

            workHourStartValue = jobIntent.getIntExtra("work_hour_start",Constants.WorkHour.START);
            setSelectedValueWorkHourStartSpinner(workHourStartValue);
            workHourStartSpinner.setEnabled(false);

            workHourEndValue = jobIntent.getIntExtra("work_hour_end",Constants.WorkHour.END);
            setSelectedValueWorkHourEndSpinner(workHourEndValue);
            workHourEndSpinner.setEnabled(false);
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        if (updateFlag) {
            setTitle(R.string.job_update);
        } else {
            setTitle(R.string.title_job_create_activity);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(!updateFlag && addressId > 0){
            try {
                Call<Address> addressCall = retroManager.deleteAddress(addressId, userId, token);
                addressCall.enqueue(new Callback<Address>() {
                    @Override
                    public void onResponse(Call<Address> call, Response<Address> response) {}

                    @Override
                    public void onFailure(Call<Address> call, Throwable t) {}
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.job_create_submit_btn:
                try {
                    saveJob();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.job_address_create_update_tv:
                Intent intent = new Intent(this, AddressFragmentActivity.class);
                if(updateFlag) {
                    intent.putExtra("update", true);
                    intent.putExtra("address_id", addressId);
                    intent.putExtra("latitude", latitude);
                    intent.putExtra("longitude", longitude);
                    intent.putExtra("address_name", addressName);
                }
                startActivityForResult(intent, Constants.ADDRESS_RESULT);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {return;}
        if (requestCode == Constants.ADDRESS_RESULT) {
            if(resultCode == Constants.ADDRESS_RESULT){
                addressName = data.getStringExtra("address_name");
                double lat = data.getDoubleExtra("latitude",0);
                latitude = lat;
                double lon = data.getDoubleExtra("longitude",0);
                longitude = lon;
                addressId = data.getIntExtra("address_id",0);
                jobAddAddressTextView.setText(addressName);
                Toast.makeText(this, "Адрес обновлен.", Toast.LENGTH_LONG).show();
            }
        }
    }


    /**
     * добавление или обновление записи
     */
    public void saveJob() throws IOException {
        jobNameValue = jobNameInputField.getText().toString();
        if (jobNameValue.isEmpty()) {
            jobNameInputField.setError(getString(R.string.err_job_name));
            return;
        }
        jobDescriptionValue = jobDescInputField.getText().toString();
        if (jobDescriptionValue.isEmpty()) {
            jobDescInputField.setError(getString(R.string.err_job_description));
            return;
        }
        String tempAmount = jobAmountInputField.getText().toString();
        jobAmountValue = !tempAmount.isEmpty() ? Integer.parseInt(tempAmount) : 0;
        if (jobAmountValue <= 0) {
            jobAmountInputField.setError(getString(R.string.err_job_amount));
            return;
        }
        if (addressId == 0) {
            Intent iAddJobAddr = new Intent(this, AddressFragmentActivity.class);
            startActivityForResult(iAddJobAddr,Constants.ADDRESS_RESULT);
            return;
        }
        if(Math.abs(workHourStartValue - workHourEndValue) < 3) {
            findViewById(R.id.work_time_layout).setBackgroundColor(Color.RED);
            Toast.makeText(JobCreateActivity.this, (R.string.err_invalid_start_end_time_diapason), Toast.LENGTH_LONG).show();
            return;
        }

        Call<Job> jobCall = null;

        if (updateFlag){
            jobCall = retroManager.updateJob(userId, token, jobId, addressId, jobNameValue, jobDescriptionValue, timeInterval, workHourStartValue, workHourEndValue, jobAmountValue);
        } else if (!updateFlag && preferencesHelper.getJobCounter() < Constants.MAX_JOB_COUNTER){
            jobCall = retroManager.addJob(userId, token, addressId, jobNameValue, jobDescriptionValue, timeInterval, workHourStartValue, workHourEndValue, jobAmountValue);
        } else {
            Toast.makeText(JobCreateActivity.this, R.string.job_over_limit_message, Toast.LENGTH_SHORT).show();
            //форсим переход назад
            finish();
            return;
        }

        jobCall.enqueue(new Callback<Job>() {
            @Override
            public void onResponse(Call<Job> call, Response<Job> response) {
                Job job = response.body();
                if (0 < job.getId()){
                    if (!updateFlag) {
                        preferencesHelper.incrementUserJobCounter();
                    }
                    Log.d(TAG, (updateFlag ? "Изменена" : "Добавлена") + " запись: addr " + addressId + " : " + jobNameValue + " : " + jobDescriptionValue);
                    Toast.makeText(JobCreateActivity.this, (updateFlag ? R.string.job_update_success : R.string.job_created), Toast.LENGTH_SHORT).show();

                    //форсим переход назад
                    finish();
                } else {
                    Log.e(TAG, getString(R.string.err_job_create));
                    Toast.makeText(JobCreateActivity.this, R.string.err_job_create, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Job> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }



    private void initWorkHourSpinners(){
        workHourStartSpinner = (Spinner)findViewById(R.id.work_hour_start_et);
        workHourEndSpinner = (Spinner) findViewById(R.id.work_hour_end_et);

        ArrayAdapter timeIntervalAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, Constants.WorkHour.AVAILABLE_HOURS);
        timeIntervalAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        workHourStartSpinner.setAdapter(timeIntervalAdapter);
        workHourStartSpinner.setSelection(Constants.WorkHour.START);

        workHourEndSpinner.setAdapter(timeIntervalAdapter);
        workHourEndSpinner.setSelection(Constants.WorkHour.END);

        workHourStartSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                workHourStartValue = Constants.WorkHour.AVAILABLE_HOURS.get(position);
                if (workHourStartValue >= workHourEndValue) {
                    workHourStartValue = Constants.WorkHour.START;
                    workHourStartSpinner.setSelection(Constants.WorkHour.START);
                    Toast.makeText(JobCreateActivity.this, (R.string.err_invalid_start_end_time), Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        workHourEndSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                workHourEndValue = Constants.WorkHour.AVAILABLE_HOURS.get(position);
                if (workHourStartValue >= workHourEndValue) {
                    workHourEndValue = Constants.WorkHour.END;
                    workHourEndSpinner.setSelection(Constants.WorkHour.END);
                    Toast.makeText(JobCreateActivity.this, (R.string.err_invalid_start_end_time), Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    /**
     * Инициализация списка интервалов времени
     */
    private void initTimeIntervalSpinner(){
        selectTimeIntervalSpinner = (Spinner) findViewById(R.id.job_time_interval_dropdown_spinner);
        timeIntervalList.add("1 час");
        timeIntervalList.add("2 часа");
        timeIntervalList.add("3 часа");
        ArrayAdapter timeIntervalAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, timeIntervalList);
        timeIntervalAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        selectTimeIntervalSpinner.setAdapter(timeIntervalAdapter);
        selectTimeIntervalSpinner.setSelection(1);// 1 час
        selectTimeIntervalSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch ((int)id){
                    case 0: timeInterval = Constants.TimeInterval._60; break;
                    case 1: timeInterval = Constants.TimeInterval._120; break;
                    case 2: timeInterval = Constants.TimeInterval._180; break;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    /*
    восстанавливает занчение выбранного интервала времени при загрузке job для обновления
     */
    private void setSelectedValueTimeIntervalSpinner(int interval){
        int i = 0;
        switch (interval){
            case Constants.TimeInterval._60: i = 0; break;
            case Constants.TimeInterval._120: i = 1; break;
            case Constants.TimeInterval._180: i = 2; break;
        }
        selectTimeIntervalSpinner.setSelection(i);
    }

    private void setSelectedValueWorkHourStartSpinner(int workHourStart){
        workHourStartSpinner.setSelection(Constants.WorkHour.AVAILABLE_HOURS.get(workHourStart));
    }
    private void setSelectedValueWorkHourEndSpinner(int workHourEnd){
        workHourEndSpinner.setSelection(Constants.WorkHour.AVAILABLE_HOURS.get(workHourEnd));
    }

}
