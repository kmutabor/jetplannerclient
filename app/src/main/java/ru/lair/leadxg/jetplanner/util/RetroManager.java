package ru.lair.leadxg.jetplanner.util;

import android.util.Log;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.lair.leadxg.jetplanner.BuildConfig;
import ru.lair.leadxg.jetplanner.service.api.*;
import ru.lair.leadxg.jetplanner.service.api.model.Address;
import ru.lair.leadxg.jetplanner.service.api.model.Event;
import ru.lair.leadxg.jetplanner.service.api.model.Job;
import ru.lair.leadxg.jetplanner.service.api.model.User;


public class RetroManager {

    private final String TAG = Constants.LOG_PREFIX + getClass().getSimpleName();

    private static RetroManager retroManager;

    private static Retrofit retrofit;

    private static JetPlannerWebApi service;

    public static RetroManager getInstance() {
        if (retroManager == null) {
            retroManager = new RetroManager();
            buildRetrofit();
            initService();
        }
        return retroManager;
    }

    private static void buildRetrofit() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.WEB_API_HOST) //TODO: решение не окончательное
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }


    private static void initService() {
        service = retrofit.create(JetPlannerWebApi.class);
    }

    private JetPlannerWebApi getService() {
        if (service == null) {
            service = retrofit.create(JetPlannerWebApi.class);
        }
        return service;
    }

    public Call<User> registration(String phone, String name) throws IOException {
        Call<User> response = getService().registration(phone, name);
        return response;
    }

    public Call<List<User>> getUsers(int user_id, String token) throws IOException {
        Call<List<User>> response = getService().getUsers(user_id, token);
        Log.d(TAG, user_id + " : " + token);

        return response;
    }

    public Call<User> deleteUser(int user_id, String token) throws IOException {
        Call<User> response = getService().deleteUser(user_id, token);
        return response;
    }


    public Call<Address> createAddress(int user_id, String token, String address, double latitude, double longitude) throws IOException {
        Call<Address> response = getService().createAddress(user_id, token, address,latitude,longitude);
        return response;
    }

    public Call<Address> updateAddress(int user_id, String token, int addressId, String address, double latitude, double longitude) throws IOException {
        Call<Address> response = getService().updateAddress(user_id, token, addressId, address,latitude,longitude);
        return response;
    }

    public Call<Address> getAddressById(int user_id, String token, int address_id) {
        Call<Address> response = getService().getAddressById(user_id, token, address_id);
        return response;
    }



    public Call<Address> deleteAddress(int address_id, int user_id, String token) throws IOException {
        Call<Address> response = getService().deleteAddress(address_id, user_id, token);
        return response;
    }


    public Call<Job> addJob(int user_id, String token, int address_id, String job_name, String job_description, int timeInterval, int workHourStart, int workHourEnd, int amount) throws IOException {
        Call<Job> response = getService().addJob(user_id, token, address_id, job_name, job_description, timeInterval, workHourStart, workHourEnd, amount);
        return response;
    }

    public Call<List<Job>> getJobsByOwnerId(int user_id, String token, int owner_id) {
        Call<List<Job>> response = getService().getJobsByOwnerId(user_id, token, owner_id);
        return response;
    }

    public Call<List<Job>> getJobsByUserId(int user_id, String token) {
        Call<List<Job>> response = getService().getJobsByUserId(user_id, token);
        return response;
    }

    public Call<List<Job>> getJobsByName(int user_id, String token, String name) {
        Call<List<Job>> response = getService().getJobsByName(user_id, token, name);
        return response;
    }

    public Call<List<Job>> getJobsByDescription(int user_id, String token, String description) {
        Call<List<Job>> response = getService().getJobsByDescription(user_id, token, description);
        return response;
    }

    public Call<Job> updateJob(int user_id, String token, int job_id, int address_id, String job_name, String job_description, int timeInterval, int workHourStart, int workHourEnd, int amount){
        Call<Job> response = getService().updateJob(user_id, token, job_id, address_id, job_name, job_description, timeInterval, workHourStart, workHourEnd, amount);
        return response;
    }

    public Call<Job> deleteJob(int job_id, int user_id, String token) throws IOException {
        Call<Job> response = getService().deleteJob(job_id, user_id, token);
        return response;
    }

    //Events
    public Call<Event> addEvent(int user_id, String token, int job_id, String status, String desired_time) throws IOException {
        Call<Event> response = getService().addEvent(user_id, token, job_id, status, desired_time);
        return response;
    }

    public Call<List<Event>> getEventsByUserId(int user_id, String token){
        Call<List<Event>> response = getService().getEventsByUserId(user_id, token);
        return response;
    }

    public Call<List<Event>> getEventsByJobId(int user_id, String token, int job_id){
        Call<List<Event>> response = getService().getEventsByJobId(user_id, token, job_id);
        return response;
    }

    public Call<List<Event>> getEventsByJobIdForClient(int user_id, String token, int job_id, boolean client){
        Call<List<Event>> response = getService().getEventsByJobIdForClient(user_id, token, job_id, client);
        return response;
    }

    public Call<Event> updateStatus(int user_id, String token, int job_id, int event_id, String event_status){
        Call<Event> response = getService().updateStatus(user_id, token, job_id, event_id, event_status);
        return response;
    }

    public Call<Event> deleteEvent(int event_id, int user_id, String token) throws IOException {
        Call<Event> response = getService().deleteEvent(event_id, user_id, token);
        return response;
    }

}
