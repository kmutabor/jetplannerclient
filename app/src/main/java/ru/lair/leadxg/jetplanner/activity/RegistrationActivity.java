package ru.lair.leadxg.jetplanner.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.lair.leadxg.jetplanner.R;
import ru.lair.leadxg.jetplanner.service.api.model.User;
import ru.lair.leadxg.jetplanner.util.Constants;
import ru.lair.leadxg.jetplanner.util.PreferencesHelper;
import ru.lair.leadxg.jetplanner.util.RetroManager;


public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener {

    private final String TAG = Constants.LOG_PREFIX + getClass().getSimpleName();

    private EditText phone = null;
    private EditText userName = null;
    private Button btnRegistration = null;

    private PreferencesHelper preferencesHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        preferencesHelper = PreferencesHelper.getInstance(getSharedPreferences(Constants.USER_DATA_PREFERENCE, MODE_PRIVATE));

        phone = (EditText) findViewById(R.id.reg_phone_et);
        userName = (EditText) findViewById(R.id.user_name_et);
        btnRegistration = (Button) findViewById(R.id.registration_btn);

        btnRegistration.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.registration_btn:
                doUserRegistration();
                break;
        }
    }


    private void doUserRegistration() {
        String phone = this.phone.getText().toString();
        if (!isValidPhone(phone)) {
            this.phone.setError(getString(R.string.err_registration_phone));
            return;
        }
        String userName = this.userName.getText().toString();
        if (!isValidUserName(userName)) {
            this.userName.setError(getString(R.string.err_registration_user_name));
            return;
        }
        RetroManager retroManager = RetroManager.getInstance();
        try {
            Call<User> response = retroManager.registration(phone, userName);
            response.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    User responseBody = (User) response.body();
                    if (null != responseBody) {
                        int userId = responseBody.getId();
                        if (userId > 0) {
                            preferencesHelper.saveUserIdAndToken(responseBody.getId(), responseBody.getToken());
                            preferencesHelper.saveUserJobCounter(0);
                            Intent i = new Intent(getApplicationContext(), MainActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                        } else {
                            RegistrationActivity.this.phone.setError(getString(R.string.err_something_wrong));
                        }
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    Log.e(TAG, t.getMessage());
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Проверяет валидность номера телефона
     *
     * @param phone
     * @return
     */
    private boolean isValidPhone(String phone) {
        String PHONE_PATTERN = "^\\d{10}$";

        Pattern pattern = Pattern.compile(PHONE_PATTERN);
        Matcher matcher = pattern.matcher(phone);
        return matcher.matches();
    }

    /**
     * @param name
     * @return
     */
    private boolean isValidUserName(String name) {
        if (name != null) {
            int l = userName.length();
            if (l <= 15 && l >= 6) {
                return true;
            }
        }
        return false;
    }

    /**
     * Проверяет валидность пароля
     * @param pass
     * @return
     */
//    private boolean isValidPassword(String pass) {
//        if (pass != null ){
//            int l = pass.length();
//            if(l >= 12 || l >= 6) {
//                return true;
//            }
//        }
//        return false;
//    }


}
