package ru.lair.leadxg.jetplanner.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.lair.leadxg.jetplanner.R;
import ru.lair.leadxg.jetplanner.service.api.model.Address;
import ru.lair.leadxg.jetplanner.util.Constants;
import ru.lair.leadxg.jetplanner.util.PreferencesHelper;
import ru.lair.leadxg.jetplanner.util.RetroManager;


public class AddressFragmentActivity extends FragmentActivity implements  View.OnClickListener, OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener {

    private final String TAG = Constants.LOG_PREFIX + getClass().getSimpleName();

    private final int PLACE_PICKER_REQUEST = 1;

    private PreferencesHelper preferencesHelper;
    private RetroManager retroManager;
    private int userId;
    private String token;
    private boolean update = false;

    private SupportMapFragment mapFragment;

    private GoogleMap map;
    private Button confirmAddressCreateButton;

    private Marker marker;

    private int addressId;

    private String addressName = "Вы здесь!";
    private double latitude = 39.0;
    private double longitude = 50.0;
    private LatLng location;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_address);

        Intent intent = getIntent();
        if(intent.hasExtra("update")){
            update = true;
            addressId = intent.getIntExtra("address_id",0);
            latitude = intent.getDoubleExtra("latitude",0);
            longitude = intent.getDoubleExtra("longitude",0);
            addressName = intent.getStringExtra("address_name");
            location = new LatLng(latitude, longitude);
        }
        startPlacePickerActivity();
    }

    private void startPlacePickerActivity(){
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        if(update || location != null){
            LatLngBounds.Builder latLngBounds = LatLngBounds.builder().include(location);
            builder.setLatLngBounds(latLngBounds.build());
        }
        try {
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                location = place.getLatLng();
                latitude = location.latitude;
                longitude = location.longitude;
                addressName = place.getAddress().toString();
                String toastMsg = String.format("Place: %s", place.toString());
                Log.e(TAG, toastMsg);


                confirmAddressCreateButton = (Button) findViewById(R.id.confirm_address_create_btn);
                confirmAddressCreateButton.setOnClickListener(this);
                mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                mapFragment.getMapAsync(this);
            }
        }
    }

    private void saveAddress() throws IOException {
        retroManager = RetroManager.getInstance();
        preferencesHelper = PreferencesHelper.getInstance(getSharedPreferences(Constants.USER_DATA_PREFERENCE, MODE_PRIVATE));
        userId = preferencesHelper.getUserId();
        token = preferencesHelper.getToken();

        Call<Address> addressCall = null;

        if (update){
            addressCall = retroManager.updateAddress(userId, token, addressId, addressName, latitude, longitude);
        } else if (!update){
            addressCall = retroManager.createAddress(userId, token, addressName, latitude, longitude);
        }

        addressCall.enqueue(new Callback<Address>() {
            @Override
            public void onResponse(Call<Address> call, Response<Address> response) {
                Address address = response.body();
                if(address != null){
                    Intent intent = new Intent();
                    intent.putExtra("address_name", address.getAddress());
                    double lat =  address.getLatitude();
                    intent.putExtra("latitude", lat);
                    double lon =  address.getLongitude();
                    intent.putExtra("longitude", lon);
                    intent.putExtra("address_id", address.getId());
                    setResult(Constants.ADDRESS_RESULT, intent);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<Address> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        UiSettings uiSettings = map.getUiSettings();
        uiSettings.setAllGesturesEnabled(false);
        uiSettings.setZoomControlsEnabled(false);
        uiSettings.setMapToolbarEnabled(false);
        uiSettings.setZoomGesturesEnabled(false);

        try {
            boolean success = map.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json));
            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

//        location = new LatLng(latitude, longitude);

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(location)
                .zoom(14)
                .build();
        map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        MarkerOptions markerOptions = new MarkerOptions()
                .position(location)
                .draggable(false)
                .snippet("Вы выбрали это место.")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                .title(addressName);
        if(marker != null){
            marker.remove();
        }
        marker = map.addMarker(markerOptions);
        marker.showInfoWindow();

        map.setOnMapClickListener(this);
        map.setOnMarkerClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case(R.id.confirm_address_create_btn):
                try {
                    saveAddress();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;

            case(R.id.map):
                startPlacePickerActivity();
                break;
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        startPlacePickerActivity();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        startPlacePickerActivity();
        return false;
    }
}
