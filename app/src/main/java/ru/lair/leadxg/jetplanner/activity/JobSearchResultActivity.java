package ru.lair.leadxg.jetplanner.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import java.util.List;

import ru.lair.leadxg.jetplanner.R;
import ru.lair.leadxg.jetplanner.adapter.JobSearchAdapter;
import ru.lair.leadxg.jetplanner.listener.recycler.RecyclerItemClickListener;
import ru.lair.leadxg.jetplanner.service.api.model.Job;
import ru.lair.leadxg.jetplanner.util.Constants;

public class JobSearchResultActivity extends AppCompatActivity {

    private final String TAG = Constants.LOG_PREFIX + getClass().getSimpleName();

    private List<Job> foundedJobs;
    private JobSearchAdapter jobSearchAdapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_search_result);

        recyclerView = (RecyclerView) findViewById(R.id.job_search_recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        Intent jobsIntent = getIntent();
        foundedJobs = (List<Job>) jobsIntent.getSerializableExtra("jobList");
        jobSearchAdapter = new JobSearchAdapter(foundedJobs);

        recyclerView.setAdapter(jobSearchAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, onItemClickListener));

    }

    private RecyclerItemClickListener.OnItemClickListener onItemClickListener = new RecyclerItemClickListener.OnItemClickListener() {
        @Override
        public void onItemClick(View view, int position) {
            Log.d(TAG, "Выбран id: " + position);
            final Job job = foundedJobs.get(position);
            if(job.getId() > 0){
                Intent iJobView = new Intent(getApplicationContext(), JobViewActivity.class);
                iJobView.putExtra("jobAboutOwner", job.getUser().getName());
                iJobView.putExtra("jobAboutName", job.getName());
                iJobView.putExtra("jobAboutAddress", job.getAddress().getAddress());
                iJobView.putExtra("jobAboutDesc", job.getDescription());
                iJobView.putExtra("timeInterval", job.getTimeInterval());
                iJobView.putExtra("jobId", job.getId());
                iJobView.putExtra("work_hour_start", job.getWorkHourStart());
                iJobView.putExtra("work_hour_end", job.getWorkHourEnd());
                startActivity(iJobView);
            }
        }
    };

}
